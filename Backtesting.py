#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import StockAnalysis
reload(StockAnalysis)


from datetime import date
from datetime import datetime
from datetime import timedelta
from pandas.io.data import DataReader
from StockAnalysis import Analysis

import time
import numpy as np
import pandas as pd
import matplotlib.dates as mdates
import matplotlib.pyplot as plt


class Backtest1(Analysis):
	
	def __init__(self,df_price,name):
		self.name = name
#		self.ref_name = ref_name
#		self.ref_price = ref_price
		self.df = df_price

		self.status = 'No action'
		self.cost = 10					# in dollars

		self.buydates = np.array([])
		self.buyprices = np.array([])
		self.selldates = np.array([])
		self.sellprices = np.array([])
		
		self.smaS = np.array([])
		self.smaL = np.array([])
		#		self.df, self.cumav, self.cumstd, self.sma = Statistics(self.df)
#		self.refret = StockAnalysis.returnCalculation(df,n=1)

		
	def Statistics(self,Nstd=7,Nsma=150):
		self.returnrate = StockAnalysis.returnCalculation(self.df,n=Nstd)
#		cumav = StockAnalysis.cumulativeMovingAverage(ret.values)
#	cumstd = StockAnalysis.cumulativeStandardDeviation(ret.values,cumav)
#	
#
#	f = np.array([df.values,sma,ret.values,cumav,cumstd])
#	dfret = pd.DataFrame(f.T,columns=['Adj Close', 'sma','return', 'cumav', 'cumstd'],index=ret.index)
#	dfret=dfret.dropna()
#	return dfret['Adj Close'], dfret['cumav'], dfret['cumstd'], dfret['sma']		
#		self.buy, self.sell = start_backtest(df_price,name)

	def STDspread(self,price):
		sma = StockAnalysis.simpleMovingAverage(df.values,Nsma)
		
	def CrossingMovingAverage(self,price,N1=20,N2=50):
#Short term sma
		if len(price) < N1:
			self.smaS = np.append(self.smaS,np.mean(price))
		else:
			self.smaS = np.append(self.smaS,np.sum(price[-N1:])/N1)
#Long term sma
		if len(price) < N2:
			self.smaL = np.append(self.smaL,np.mean(price))
		else:
			self.smaL = np.append(self.smaL,np.sum(price[-N2:])/N2)
# Setting buy and sell flag
		if len(self.smaS) > 1:
			if self.smaS[-2] > self.smaL[-2] and self.smaS[-1] < self.smaL[-1]:
				self.status = 'Buy'
			elif self.smaS[-2] < self.smaL[-2] and self.smaS[-1] > self.smaL[-1] and len(self.buyprices)>0:
				self.status = 'Sell'
			else:
				self.status = 'No action'


	def strategy(self,price,stratname):
		if stratname == 'STDspread':
			self.STDspread(price)						
		if stratname == 'CrossingMovingAverage':
			self.CrossingMovingAverage(price)		
			
	def execution(self,price):
		n = len(price)-1
		if self.status == 'Buy':
			self.buydates = np.append(self.buydates,self.df.index[n])
			self.buyprices = np.append(self.buyprices,price[-1])
						
		if self.status == 'Sell':
			self.selldates = np.append(self.selldates,self.df.index[n])
			self.sellprices = np.append(self.sellprices,price[-1])	

			
#def start_backtest(df,name):
	
	
#	fig = plt.figure()
#	plt.xlim([df.index[1], df.index[-1]])
#	plt.ion()
#	plt.show()
#	
#	
#	buytick='Sell needed'
#	for i in (np.arange(1,len(df))):
##		print df.values[i]-buyprices[-1], ">?", cumstd[i]*df.values[i]


#		if buytick=='Sell needed' and df.values[i]-buyprices[-1]>cumstd[i]*df.values[i]:
#			selldates = np.append(selldates,df.index[i])
#			sellprices = np.append(sellprices,buyprices[-1]+cumstd[i]*df.values[i])
#			buytick = 'Buy needed'
#
#		if buytick=='Sell needed' and df.values[i]<sma[i]:
#			selldates = np.append(selldates,df.index[i])
#			sellprices = np.append(sellprices,df.values[i])
#			buytick = 'Buy needed'
#
#		if buytick=='Buy needed' and df.values[i]<sellprices[-1]-cumstd[i]*df.values[i]:
#			buydates = np.append(buydates,df.index[i])
#			buyprices = np.append(buyprices,sellprices[-1]-cumstd[i]*df.values[i])
#			buytick = 'Sell needed'

#		if buytick=='Buy needed' and df.values[i]>sma[i]:
#			buydates = np.append(buydates,df.index[i])
#			buyprices = np.append(buyprices,df.values[i])
#			buytick = 'Sell needed'
		
#		fig.clear()
#		plt.plot(df.index[:i],df[:i],'b',label=name)
#		plt.plot(df.index[:i],sma[:i],'g',label='SMA(150)')
#		plt.plot(buydates,buyprices,'gs',label='buy')
#		plt.plot(selldates,sellprices,'rs',label='sell')
#		plt.legend()
#		plt.draw()
##		time.sleep(0.00000005)
#	if len(sellprices) < len(buyprices):
#		retur=sellprices-buyprices[:-1]
#		rstd = retur.std()
#		sharpe = (np.sum(retur)-refret.sum())/rstd
#		print "Return: ", np.sum(retur)
#		print "Sharpe: ", sharpe
#	else:
#		print "Return: ",np.sum(sellprices-buyprices)		
#	return buyprices, sellprices
#
#
	

