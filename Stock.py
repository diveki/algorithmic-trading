#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import StockAnalysis
reload(StockAnalysis)
import Backtesting
reload(Backtesting)
import portfoliohu
reload(portfoliohu)

from lxml.html import parse
from urllib2 import urlopen
from datetime import date
from datetime import datetime
from datetime import timedelta
from httplib2 import Http
from urllib import urlencode
from decimal import Decimal
from pandas.io.data import DataReader

from StockAnalysis import Analysis
from Backtesting import Backtest1

import string, os
import numpy as np
import pandas as pd




class Stock(Analysis,Backtest1):
	def __init__(self,name='GSPARK:BUD',pathname = ''):
		self.name = name
		if pathname == '':
			self.pathname = '/media/zsoltlinux/New Volume/Uzlet/Tozsde/algorithmic-trading/Stocks'
		else:
			self.pathname = pathname
		self.sday = '2000-01-01'
		self.eday = date.today().strftime('%Y-%m-%d')
		self.created = 0
		
	def unpack(self,row, kind='td'):
		cols = row.findall('.//%s' % kind)
		return [values.text_content() for values in cols]

	def ReadStockFromYahoo(self,dateb,datee):
		by = int(dateb.split('-')[0])
		bm = int(dateb.split('-')[1])
		bd = int(dateb.split('-')[2])
		ey = int(datee.split('-')[0])
		em = int(datee.split('-')[1])
		ed = int(datee.split('-')[2])
		beg = datetime(by,bm,bd)
		end = datetime(ey,em,ed)
		try:
			datf = DataReader(self.name,  "yahoo", beg, end)
			if self.created == 1:
				self.df=self.df.append(datf)
			else:
				self.df = datf
		except IOError:
			print "Yahoo prevented download!"
			if self.created == 1:
				self.df=self.df
		return self.df
	
	def download(self,portal,name='GSPARK:BUD',start_day='2000-01-01',end_day=date.today().strftime('%Y-%m-%d')):
		self.name = name
		self.portal = portal
		start_day, end_day = self.existance_verification()
		if self.portal == 'ft':
			return self.ft_download(start_day,end_day)
		elif self.portal == 'portfolio':
			return self.portfolio_download(start_day,end_day)
		elif self.portal == 'yahoo':
			return self.ReadStockFromYahoo(start_day,end_day)
		else:
			print '"%s" portal is not found in the database' % portal

	def existance_verification(self):
		'''
		This function verifies wether the given stock has been 			downloaded yet or not. If yes, it modifies the start and 		end date in order to download those days that are 				missing
		'''
		dirlist = os.listdir(self.pathname)
		if dirlist == []:
			sday = self.sday
			enday = self.eday 
			return sday, enday
		elif self.name in dirlist:
			self.savename = os.path.join(self.pathname,self.name,self.name+'.xlsx')
			self.df = pd.read_excel(self.savename, na_values=['NA'])
			if type(self.df.index[-1])==type(u's'):
				tmp = np.array([])
				for i in np.arange(1,len(self.df)):
					tmp = np.append(tmp,self.df.index[i].encode('ascii'))
				tmp=[datetime.strptime(x,'%Y-%m-%d') for x in tmp]
				self.df = pd.DataFrame(self.df.values[1:],index=tmp)
			beg = self.df.index[-1]+timedelta(days=1)
			sday = beg.strftime('%Y-%m-%d')
			enday = self.eday
			self.created = 1
			return sday, enday
		else:
			return self.sday, self.eday
	
	def ft_download(self,start_day,end_day):
		''' Possible names:
		GSPARK:BUD - Graphisoft park
		'''
		urladress = 'http://markets.ft.com/research//Tearsheets/PriceHistoryPopup?symbol=' + self.name
		quote = parse(urlopen(urladress))
		tables = quote.getroot().findall('.//table')[2]
		header = self.unpack(tables,kind='th')
		rows = tables.findall('.//tr')
		prices = [self.unpack(r) for r in rows]
		return prices, header, tables
		
	def portfolio_download(self,start_day='2000-01-01',end_day=date.today().strftime('%Y-%m-%d')):
		df_stock=portfoliohu.stocklist()
		dfref = df_stock[df_stock.index==self.name]
		tickid = dfref[0].values[0]
		if self.sday < dfref[1].values[0]:
			start_day = dfref[1].values[0]
		else:
			start_day = self.sday
		end_day = self.eday
		url = 'http://www.portfolio.hu/history/reszveny-adatok.php'
		method = 'POST'
		data = {}
		tick = '%s:%s:%s' % (tickid,start_day,end_day)
		name = self.name
		data[name] =  {'tipus': '1',
				'ticker': tick,
				'startdate': start_day,
				'enddate': end_day,
				'open': '1',
				'close': '1',
				'text': 'szövegfájl'}
		d = urlencode ( data [ name ] )
		http = Http ()
		r, text = http . request ( url, method, d, headers = { 'Content-type': 'application/x-www-form-urlencoded' } )
		tmp = text.rstrip().split('\n')
		idx = np.array([])
		openprice = np.array([])
		closeprice = np.array([])
		
		for ii in np.arange(len(tmp)-1):
			if ii > 2:
				x=[]
				tmp[ii] = tmp[ii].split(' ')
				for jj in np.arange(len(tmp[ii])):
					if tmp[ii][jj] != '':
						x.append(tmp[ii][jj])
				idx = np.append(idx,datetime.strptime(x[0],'%Y-%m-%d'))
				openprice=np.append(openprice,float(x[1]))
				closeprice=np.append(closeprice,float(x[2]))
		dftmp = np.array([openprice,closeprice])
		df = self.construct_dataframe(idx,dftmp)
		return df

	def construct_dataframe(self,date,price,label1='Date',label2=['Price']):
		self.df = pd.DataFrame(price.T,index=date,columns=['Open','Adj Close'])
		return self.df
		
	def save_prices_to_excel(self):
		dirlist = os.listdir(self.pathname)
		if dirlist == []:
			os.mkdir(self.pathname +  self.name) 
		elif self.name in dirlist:
			savename = os.path.join(self.pathname,self.name,self.name+'.xlsx')
			self.df.to_excel(savename, sheet_name='Sheet1', engine='xlsxwriter')
			return 'Prices has been saved!'
		else:
			os.mkdir(os.path.join(self.pathname,self.name))
			savename = os.path.join(self.pathname,self.name,self.name+'.xlsx')
			self.df.to_excel(savename, sheet_name='Sheet1', engine='xlsxwriter')
			return 'Prices has been saved!'
