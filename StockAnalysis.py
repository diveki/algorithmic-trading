#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import matplotlib.pyplot as plt
import numpy as np
import datetime
import matplotlib.dates as mdates
import pandas as pd
import pandas.io.data as web

class Analysis():

##################################				
				


##################################
		
	def plot_prices(self,**kwargs):
		''' 
		Plots the Adj Close prices
		- start = (yyyy,mm,dd) - start date to plot from
		- end = (yyyy,mm,dd) - end date to plot until
		- days = 'on' - plots the x axis in days instead of months
		- BuySell = 'on' - plots where trade happened
		'''
# Creating buy and sell prices
		self.buydates = [datetime.datetime(2004,4,14), datetime.datetime(2004,5,5)]
		self.buyprices = [1128.17,1121.53]
		self.selldates = [datetime.datetime(2004,10,15), datetime.datetime(2004,11,15)]
		self.sellprices = [1108.2,1183.81]
		
# determining start date of the plot
		if kwargs.has_key('start'):
			sidx = datetime.datetime(kwargs['start'][0],kwargs['start'][1],kwargs['start'][2])
			startidx=np.where(self.df.index==sidx)[0][0]
		else:
			startidx = 1
# determining end date of the plot
		if kwargs.has_key('end'):
			eidx = datetime.datetime(kwargs['end'][0],kwargs['end'][1],kwargs['end'][2])
			endidx=np.where(self.df.index==eidx)[0][0]
		else:
			endidx = -1
# determining the interval frequency of the x axis		
		inter = (self.df.index[endidx] - self.df.index[startidx])
		if kwargs.has_key('days'):
			interv = inter.days/(30)
			xloc = mdates.DayLocator(interval=interv)
		else:
			interv = inter.days/365
			xloc = mdates.MonthLocator(interval=interv) # every month
		
# Plotting the prices
		fig, ax = plt.subplots()
		ax.plot(self.df.index[startidx:endidx], self.df['Adj Close'][startidx:endidx],'-', label=self.name)

# Plotting buy end sell positions
		if kwargs.has_key('BuySell'):
			ax.plot(self.buydates,self.buyprices,'sr',label='Buy')
			ax.plot(self.selldates,self.sellprices,'og',label='Sell')
			
# Plotting simple moving avarage positions
		if kwargs.has_key('SMA'):
			s = kwargs['SMA']
			sma = simpleMovingAverage(self.df['Adj Close'][startidx:endidx],s)
			ax.plot(self.df.index[startidx:endidx],sma,'--r',label='SMA(%s)'%s)

# Plotting simple moving avarage positions
		if kwargs.has_key('CMA'):
			cma = cumulativeMovingAverage(self.df['Adj Close'][startidx:endidx])
			ax.plot(self.df.index[startidx:endidx],cma,'--g',label='CMA')

		ax.xaxis.set_major_locator(xloc)
		ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %d %Y'))
#		ax.set_xlim(self.df.index[1], self.df.index[-1])
		ax.grid(True)
		fig.autofmt_xdate(rotation=90)
		plt.xlabel('Month/Year')
		plt.ylabel('Price ($)')
		plt.title('%s Daily Prices' % (self.name))
		plt.legend()
		plt.show()
		return "Plot is done!"
##################################

def simpleMovingAverage(vector,size):
		'''
		Moving avarage calculation:
		- vector - the data that you want to trace
		- size - the portion of the vector that you want to trace
		'''
		ma = np.empty(0)
		temp = np.empty(0)
		for ii in np.arange(vector.shape[0]):
			if ii <= size:
				temp=np.append(temp,vector[ii])
				ma=np.append(ma,np.mean(temp))
			else:
				temp=np.append(temp,vector[ii])
				ma=np.append(ma,ma[ii-1]+(temp[-1]-temp[0])/size)
				temp=np.delete(temp,0)
		return ma
		
##################################		

def cumulativeMovingAverage(vector,*args):
		'''
		Cumulative moving avarage calculation:
		- vector - the data that you want to trace
		- *args - a second parameter in case when the iteration is run outside this code. The second argument should be CMA[i-1]
		'''
		if len(args)==1:
			n = len(args[0])
			if n == 0:
				return vector
			else:
				return (vector + n*args[0][-1])/(n+1)
		else:
			cma = np.empty(0)
			n=0
			for ii in np.arange(vector.shape[0]):
				if np.isnan(vector[ii]):
					cma=np.append(cma,vector[ii])
				else:
					n=n+1
					if ii == 0:
						cma=np.append(cma,vector[ii])
					else:
						if np.isnan(vector[ii-1]):
							cma=np.append(cma,vector[ii])
						else:
							temp = (vector[ii]+(n-1)*cma[ii-1])/n
							cma=np.append(cma,temp)
		return cma
			
##################################		

def cumulativeStandardDeviation(vector,mean,*args):
		'''
		Cumulative standard deviation calculation:
		- vector - the data that you want to trace
		- *args: a second parameter in case when the iteration is run outside this code.
		- args[0] - CSTD vector containing all the previous elements [0:i-1]
		- mean - CMA vector containing all elements, CMA[0:i]
		'''
		if len(args)==1:
			n = len(args[0])
			if n == 0:
				return 0
			else:
				tmpstd = np.square(args[0][-1])*(n-1)/n+np.power(vector,2)/n
				tmpav = np.square(mean[-2])-np.square(mean[-1])*(n+1)/n
				return np.sqrt((tmpstd+tmpav))
		else:
			cma = np.empty(0)
			n=-1
			for ii in np.arange(vector.shape[0]):
				if np.isnan(vector[ii]):
					cma=np.append(cma,vector[ii])
				else:
					n=n+1
					if ii == 0:
						cma=np.append(cma,0)
					else:
						if np.isnan(vector[ii-1]):
							cma=np.append(cma,0)
						else:
							tmpstd = np.square(cma[ii-1])*(n-1)/n+np.power(vector[ii],2)/n
							tmpav = np.square(mean[ii-1])-np.square(mean[ii])*(n+1)/n
							cma=np.append(cma,np.sqrt(tmpstd+tmpav))
			return cma

##################################

def returnCalculation(df,n=1):
	''' 
	Returns relative return on the time series
	
	- date - is the date corresponding to each price tick
	- price - is the time series
	- n - specifies the interval for return calculation; n=1 means day to day return
	''' 
	ret = (df-df.shift(n))/df.shift(n)
	return ret

##################################

#def SharpeRatio(buy,sell):
