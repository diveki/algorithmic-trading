from lxml.html import parse
from urllib2 import urlopen
from datetime import date

def _unpack(row, kind='td'):
	cols = row.findall('.//%s' % kind)
	return [values.text_content() for values in cols]


def ft_download(name,start_day='2000-01-01',end_day=date.today()):
	''' Possible names:
	GSPARK:BUD - Graphisoft park
	'''
	urladress = 'http://markets.ft.com/research//Tearsheets/PriceHistoryPopup?symbol=' + name
	quote = parse(urlopen(urladress))
	tables = quote.getroot().findall('.//table')[2]
	header = _unpack(tables,kind='th')
	rows = tables.findall('.//tr')
	prices = [_unpack(r) for r in rows]
	return prices, header, tables
