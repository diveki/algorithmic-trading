from pandas.io.data import DataReader
from datetime import datetime
import matplotlib.pyplot as plt

def ReadStockFromYahoo(dateb,datee,*var_str):
    goog = {}
    for str1 in var_str:
        goog[str1] = DataReader(str1,  "yahoo", dateb, datee)
    return goog

    
if __name__ == '__main__':
    str1 = ("^GSPC","^IXIC","AAPL","BAC")#"GBPHUF=X")
    db = datetime(2011,2,1)
    de = datetime(2012,1,1)
    plt.figure();
    plt.plot(True)
    for stock in str1:
        test = ReadStockFromYahoo(db,de,stock)
        test[stock]["Adj Close"].plot(label = stock);
        plt.show();
    plt.legend()